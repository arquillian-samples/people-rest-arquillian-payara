package mz.vodacom.people.rest.resources;


import mz.vodacom.people.Utils;
import mz.vodacom.people.persistence.Person;
import mz.vodacom.people.service.PeopleService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Path("/person")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PersonRestResource {

    @Inject
    private PeopleService service;


    @POST
    @Produces(MediaType.TEXT_PLAIN)
    public String create(PersonDto dto){

        return service.create(dto.getName(),dto.getSurname(),
                dto.getEmail(),
                Utils.parseDate(dto.getBirthDate()));

    }


    @GET
    public List<PersonDto> allPeople(){
        return convert(service.everyone());
    }


    @GET
    @Path("/{personId}")
    public Response findPerson(@PathParam("personId") String personId){

        Optional<Person> personOptional = service.getPerson(personId);
        if(!personOptional.isPresent())
            return Response.status(Response.Status.NOT_FOUND)
                    .build();

        return Response.ok(personOptional.get())
                .build();

    }

    @GET
    @Path("/age/teenagers")
    public List<PersonDto> teenagers(){

        return convert(service.everyTeenager());

    }

    @GET
    @Path("/age/older/{age}")
    public List<PersonDto> older(@PathParam("age") int age){

        return convert(service.everyoneWithAgeGreaterThan(
                age));

    }


    @GET
    @Path("/age/younger/{age}")
    public List<PersonDto> younger(@PathParam("age") int age){

        return convert(service.everyoneWithAgeLessThan(
                age));

    }


    private List<PersonDto> convert(Collection<Person> personCollection){
        return personCollection.stream().map(entity -> {
            PersonDto dto = new PersonDto();
            dto.setId(entity.getId());
            dto.setName(entity.getName());
            dto.setSurname(entity.getSurname());
            dto.setEmail(entity.getEmail());
            dto.setBirthDate(Utils.format(entity.getBirthDate()));
            return dto;
        }).collect(Collectors.toList());
    }



}
