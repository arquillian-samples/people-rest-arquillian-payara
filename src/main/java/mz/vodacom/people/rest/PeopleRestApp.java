package mz.vodacom.people.rest;

import mz.vodacom.people.rest.resources.PersonRestResource;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/api")
public class PeopleRestApp extends Application {

    public Set<Class<?>> getClasses() {
        Set<Class<?>> classes= new HashSet<>();
        classes.add(PersonRestResource.class);
        return classes;
    }


}
