package mz.vodacom.people.rest;

public class RestException extends RuntimeException {

    private int code;

    public RestException(String message, int code){
        super(message);
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
