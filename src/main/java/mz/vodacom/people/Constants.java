package mz.vodacom.people;

import java.util.regex.Pattern;

public final class Constants {

    public static final Pattern VALID_EMAIL_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public static final String SIMPLE_DATE_FORMAT = "yyyy/MM/dd";

}
