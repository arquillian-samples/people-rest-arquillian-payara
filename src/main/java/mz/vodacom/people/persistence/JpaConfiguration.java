package mz.vodacom.people.persistence;

//import org.emerjoin.cdi.methodcontext.MethodScoped;

import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.transaction.TransactionScoped;

public class JpaConfiguration {

    private static EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence.createEntityManagerFactory("people");

    @Produces @TransactionScoped //@Dependent
    public EntityManager produceEntityManager(){

        System.err.println("Creating new Entity Manager...");
        return ENTITY_MANAGER_FACTORY.createEntityManager();

    }


    public void destroyEntityManager(@Disposes EntityManager entityManager){
        System.err.println("Disposing entity manager...");
        if(!entityManager.isOpen())
            return;
        entityManager.close();
    }


}
