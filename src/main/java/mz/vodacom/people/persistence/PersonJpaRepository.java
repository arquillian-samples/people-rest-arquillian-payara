package mz.vodacom.people.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.cdi.Eager;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface PersonJpaRepository extends JpaRepository<Person,String> {

    @Query("select p from Person p where p.birthDate < :date")
    List<Person> findWithBirthDateLessThan(@Param("date") Date date);

    @Query("select p from Person p where p.birthDate between :start AND :end")
    List<Person> findWithBirthDateBetween(@Param("start") Date start, @Param("end") Date end);

    @Query("select p from Person p where p.birthDate > :date")
    List<Person> findWithBirthDateGreaterThan(@Param("date") Date date);

}
