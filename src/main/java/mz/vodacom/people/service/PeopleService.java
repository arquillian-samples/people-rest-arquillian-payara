package mz.vodacom.people.service;


import mz.vodacom.people.Constants;
import mz.vodacom.people.persistence.Person;
import mz.vodacom.people.persistence.PersonJpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Transactional
@ApplicationScoped
public class PeopleService {

    @Inject
    private PersonJpaRepository personRepository;

    public String create(String name, String surname, String email, Date birthDate) {
        if(name==null||name.isEmpty())
            throw new IllegalArgumentException("name must not be null nor empty");
        if(surname==null||surname.isEmpty())
            throw new IllegalArgumentException("surname must not be null nor empty");
        if(email==null||email.isEmpty())
            throw new IllegalArgumentException("email must not be null nor empty");
        if(!Constants.VALID_EMAIL_REGEX.matcher(email).matches())
            throw new IllegalArgumentException("invalid email format: "+email);
        try {
            Person person = new Person();
            person.setName(name);
            person.setSurname(surname);
            person.setBirthDate(birthDate);
            person.setEmail(email);

            personRepository.save(person);
            return person.getId();

        }catch (Exception ex){
            throw new ServiceException("unexpected error found persisting person object",
                    ex);
        }
    }




    public List<Person> everyone(){
        try {
            return personRepository.findAll();
        }catch (Exception ex){
            throw new ServiceException("unexpected error found while listing people",
                    ex);
        }
    }


    private Date getBirthYear(int age){
        Calendar calendar= Calendar.getInstance();
        calendar.add(Calendar.YEAR,-age);
        Date date = calendar.getTime();
        System.out.println("Birth-date for ["+age+"] years = "+date);
        return calendar.getTime();
    }


    public List<Person> everyoneWithAgeGreaterThan(int age){

        try {
            return personRepository.findWithBirthDateLessThan(
                    getBirthYear(age));
        }catch (Exception ex){
            throw new ServiceException(
                    String.format("unexpected error found while listing people with less than %d years",
                    age),ex);
        }

    }


    public List<Person> everyoneWithAgeLessThan(int age){
        return personRepository.findWithBirthDateGreaterThan(
                getBirthYear(age));
    }


    public List<Person> everyTeenager(){
        Date twelveYear = getBirthYear(12);
        Date twentyYear = getBirthYear(20);
        return personRepository.findWithBirthDateBetween(twentyYear,
                twelveYear);
    }


    public Optional<Person> getPerson(String personId){

        return personRepository.findById(
                personId);

    }

}
