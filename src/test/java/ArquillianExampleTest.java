import com.vodacom.test.AppScopedExample;
import mz.vodacom.people.Constants;
import mz.vodacom.people.ExampleBean;
import mz.vodacom.people.Utils;
import mz.vodacom.people.persistence.JpaConfiguration;
import mz.vodacom.people.persistence.Person;
import mz.vodacom.people.persistence.PersonJpaRepository;
import mz.vodacom.people.persistence.UUIDGenerator;
import mz.vodacom.people.rest.PeopleRestApp;
import mz.vodacom.people.rest.RestException;
import mz.vodacom.people.rest.resources.PersonDto;
import mz.vodacom.people.rest.resources.PersonRestResource;
import mz.vodacom.people.service.PeopleService;
import mz.vodacom.people.service.ServiceException;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.asset.FileAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.io.File;
import java.util.Calendar;
import java.util.List;

@RunWith(Arquillian.class)
public class ArquillianExampleTest {

    @Inject
    private ExampleBean exampleBean;

    @Inject
    private PeopleService peopleService;

    @Deployment
    public static WebArchive createDeployment() {
        File[] libs = Maven.resolver()
                .loadPomFromFile("pom.xml").importRuntimeDependencies().resolve().withTransitivity()
                .asFile();

        WebArchive jar =  ShrinkWrap.create(WebArchive.class,"test.war")
                //Persistence
                .addClass(JpaConfiguration.class)
                .addClass(Person.class)
                .addClass(PersonJpaRepository.class)
                .addClass(UUIDGenerator.class)
                //Service
                .addClass(ServiceException.class)
                .addClass(PeopleService.class)
                //Rest
                .addClass(RestException.class)
                .addClass(PersonDto.class)
                .addClass(PersonRestResource.class)
                .addClass(PeopleRestApp.class)
                //Others
                .addClass(Constants.class)
                .addClass(Utils.class)
                .addClass(ExampleBean.class)

                .addAsLibraries(libs)
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .add(new FileAsset(new File("src/test/resources/persistence.xml")),"/WEB-INF/classes/META-INF/persistence.xml")
                .setWebXML("web.xml");

        //System.out.println(jar.toString(true));
        return jar;
    }

    @Test
    public void say_hello() {
        System.out.println("Hello worked: "+ exampleBean.hello());
    }

    @Test
    public void people_service() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR,1993);
        calendar.set(Calendar.MONTH,Calendar.SEPTEMBER);
        calendar.set(Calendar.DATE,15);

        String personId = peopleService.create("Mario","Junior",
                "francisco.junior.mario@gmail.com",
                calendar.getTime());
        List<Person> people = peopleService.everyone();
        Assert.assertEquals(1,people.size());
    }

}
